from functools import reduce
from itertools import product
from collections import namedtuple, deque

class DFA(namedtuple('DFA', ['initial', 'transitions', 'finals', 'states'])):
    def next(self, state, character):
        if (state,character) in self.transitions:
            return self.transitions[(state, character)]

    def accept(self,state):
        return state in self.finals
        
    def alphabet(self):
        return {character for (state, character) in self.transitions}

class NDFA(namedtuple('NDFA', ['initial', 'transitions', 'finals', 'states'])):
    def next(self, states, character):
        sets = (self.transitions[(state, character)] for state in states
                if (state,character) in self.transitions)
        return reduce(set.union, sets, set())

    def is_epsilon_free(self):
        return not bool({character for (state, character)
                        in self.transitions
                        if character == '&'})

    def accept(self,states):
        if not self.is_epsilon_free():
            raise ValueError("Automaton is not epsilon free.")
        print(states, self.finals)
        return bool(states & self.finals)

    def alphabet(self):
        return {character for (state, character) in self.transitions if character != '&'}

def epsilon_closure(state, transitions):
    epsilon_transitions = {s : transitions[(s, '&')]
                           for (s, character) in transitions
                           if character == '&'}

    queue = deque([state])
    visited = {state}

    while queue:
        current = queue.pop()
        if current in epsilon_transitions:
            to_add = {s for s in epsilon_transitions[current]
                      if s not in visited}
            visited |= to_add
            queue.extendleft(to_add)

    return visited - {state}

def epsilon_free(automaton):
    initial, transitions, finals, states = automaton

    new_finals = {state for state in states
                  if state in finals or
                  (epsilon_closure(state,transitions) & finals)}

    old_transitions = {(state,character):transitions[(state,character)]
                       if (state, character) in transitions
                       else set()
                       for (state, character) in product(states, automaton.alphabet())}

    new_transitions = old_transitions.copy()

    for (state,character) in old_transitions:
        for other in epsilon_closure(state, transitions):
            new_transitions[(state, character)] = (new_transitions[(state, character)] |
                                                   old_transitions[(other, character)])

    new_transitions = {(state,character):new_transitions[(state,character)]
                       for (state,character) in new_transitions
                       if new_transitions[(state,character)]}

    return NDFA(initial, new_transitions, new_finals, states)


def blind_check(automaton, string):
    return automaton.accept(reduce(automaton.next, string, automaton.initial))

def check_string(automaton, string):
    try:
        return blind_check(automaton, string)
    except ValueError:
        return blind_check(epsilon_free(automaton), string)

test = DFA('q0',
           {('q0', 'a') : 'q1',
            ('q1', 'a') : 'q0'},
           {'q0'},
           {'q0', 'q1'})

testnd = NDFA({'q0'},
              {('q0', 'a'): {'q0', 'q1'},
               ('q1', 'a'): {'q0', 'q2'}},
              {'q1'},
              {'q0', 'q1', 'q2'})

testepsilon = NDFA({'q0'},
                   {('q0', '&') : {'q1', 'q3'},
                    ('q1', 'a') : {'q2'},
                    ('q2', 'a') : {'q1'},
                    ('q3', 'b') : {'q4'},
                    ('q4', 'b') : {'q3'}},
                   {'q1', 'q3'},
                   {'q0', 'q1', 'q2', 'q3', 'q4'})
